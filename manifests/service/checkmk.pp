# Put a check_mk service check in place for a specific tunnel.
#
define stunnel::service::checkmk () {

  case $::operatingsystem {
    'Debian': {
      case $::operatingsystemmajrelease {
        '7': {
          # wheezy and older stunnels used 6 processes by default
          check_mk::agent::ps { "stunnel_${name}":
            procname => "~/usr/bin/stunnel4 /etc/stunnel/${name}\\.conf",
            levels   => '6, 6, 6, 6'
          }
        }
        default: {
          # jessie and newer use a single process
          check_mk::agent::ps { "stunnel_${name}":
            procname => "~/usr/bin/stunnel4 /etc/stunnel/${name}\\.conf",
            levels   => '1, 1, 1, 1'
          }
        }
      }
    }
    default: {
      check_mk::agent::ps { "stunnel_${name}":
        # default to a single process
        procname => "~/usr/bin/stunnel4 /etc/stunnel/${name}\\.conf",
        levels   => '1, 1, 1, 1'
      }
    }
  }
}
